{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import           Data.Map  as M (Map, insert, lookup, size)
import           Text.Read (readMaybe)

data Player = X | O deriving (Show, Eq)
data Row = R1 | R2 | R3 deriving (Eq, Ord, Show, Enum, Bounded)
data Col = C1 | C2 | C3 deriving (Eq, Ord, Show, Enum, Bounded)
type Board = Map (Row,Col) Player

universe :: (Bounded a, Enum a) => [a]
universe = [minBound..maxBound]

printBoard :: Board -> IO ()
printBoard b = putStrLn "" >> mapM_ printRow universe >> putStrLn "" where
  printRow r = putStrLn . (" " <>) . unwords
    $ (\c -> maybe "□" show $ M.lookup (r,c) b) <$> universe

clear :: IO ()
clear = putStr "\ESC[1J"

getInput :: forall a. (Bounded a, Enum a) => Player -> Board -> String -> IO a
getInput p b msg = do
  putStrLn ("Turn: " <> show p) >> printBoard b >> putStrLn ("Enter " <> msg)
  inp <- getLine; clear
  let minVal = fromEnum (minBound :: a); maxVal = fromEnum (maxBound :: a)
  case subtract 1 <$> readMaybe inp of
    Just r | minVal <= r && r <= maxVal -> pure $ toEnum r
    Just _  -> putStrLn (mconcat
      [ msg, " must be ", show (minVal + 1), " - ", show (maxVal + 1)]) >> getInput p b msg
    Nothing -> putStrLn ("\"" <> inp <> "\" is not a number")           >> getInput p b msg

game :: Player -> Board -> IO ()
game p b = do
  k <- (,) <$> getInput p b "Row" <*> getInput p b "Column"
  let b' = insert k p b in case M.lookup k b of
    Nothing | checkWinner b' p -> putStrLn (show p <> " won!") >> printBoard b'
            | size b' == 9     -> putStrLn "Draw"              >> printBoard b'
            | otherwise        -> game (case p of X -> O; O -> X) b'
    Just p'                    -> putStrLn ("Space taken by " <> show p' <> ", try again") >> game p b

checkWinner :: Board -> Player -> Bool
checkWinner b p = let check k = M.lookup k b == Just p in or . mconcat $
  [ (\r -> all (\c -> check (r,c)) universe) <$> universe
  , (\c -> all (\r -> check (r,c)) universe) <$> universe
  , all check <$> [ zip universe universe, zip universe $ reverse universe ] ]

main :: IO ()
main = clear >> putStrLn "Welcome! to Tic Tac Toe" >> game X mempty
